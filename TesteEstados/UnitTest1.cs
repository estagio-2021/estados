using System;
using System.Collections.Immutable;
using estados;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace TesteEstados
{
    public class UnitTest1 : ItemsControllerTest
    {
        
        public UnitTest1() : base( 
            new DbContextOptionsBuilder<EstadoDbContext>()
            .UseSqlite("Filename=Test.db")
            .Options)
        {
        }
        
        [Fact]
        public void Can_get_items()
        {
            using var context = new EstadoDbContext(ContextOptions);
            
            var controller = new EstadosController(context);

            var immutableList = controller.Get();
                
            Assert.Equal(27, immutableList.Count);
        }
    }
}