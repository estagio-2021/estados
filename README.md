# API de unidades federativas

## Task list
- [x] escrever app

- [x] banco de dados

- [x] banco de dados docker-compose

- [x] dockerfile da aplicacao

- [x] docker-compose com aplicacao

- [ ] test

- [ ] gitlab-ci.yaml

- [ ] config - heroku


## Glossário

```shell
dotnet add package Microsoft.EntityFrameworkCore.Design --version 5.0.11

dotnet tool install --global dotnet-ef
```


```plantuml
class EstadosController{

    -  bd : EstadoDbContext
    -  _logger : EstadoDbContext
    +  Estados[] Get()
}

class EstadoDbContext{

    +  Estados:  Estados[]

}

class Estados{

      + Id : int 
      + Uf : string 
      + Nome : string 
}

EstadosController .. Estados
EstadosController o-- EstadoDbContext
Estados -- EstadoDbContext

```
# Arquivos de Configuração

[docker-compose](docker-compose.yaml)

[Dockerfile](Dockerfile)

[appsettings.Staging](./webapi-estados/appsettings.Staging.json)

[Colecao Postman](API-Estados.json)