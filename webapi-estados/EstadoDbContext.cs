using System.Runtime.CompilerServices;
using Microsoft.EntityFrameworkCore;

namespace estados
{
    public class EstadoDbContext : DbContext
    {
      
        public DbSet<Estados> Estados { get; set; }

        public EstadoDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Estados>().HasData(
                        new Estados() { Id = 1, Nome= "Acre", Uf = "AC" },
                        new Estados() { Id = 2, Nome= "Alagoas", Uf = "AL" },
                        new Estados() { Id = 3, Nome= "Amapá", Uf = "AP" },
                        new Estados() { Id = 4, Nome= "Amazonas", Uf = "AM" },
                        new Estados() { Id = 5, Nome= "Bahia", Uf = "BA" },
                        new Estados() { Id = 6, Nome= "Ceará", Uf = "CE" },
                        new Estados() { Id = 7, Nome= "Distrito Federal", Uf = "DF" },
                        new Estados() { Id = 8, Nome= "Espírito Santo", Uf = "ES" },
                        new Estados() { Id = 9, Nome= "Goiás", Uf = "GO" },
                        new Estados() { Id = 11, Nome= "Mato Grosso", Uf = "MT" },
                        new Estados() { Id = 10, Nome= "Maranhão", Uf = "MA" },
                        new Estados() { Id = 12, Nome= "Mato Grosso do Sul", Uf = "MS" },
                        new Estados() { Id = 13, Nome= "Minas Gerais", Uf = "MG" },
                        new Estados() { Id = 14, Nome= "Pará", Uf = "PA" },
                        new Estados() { Id = 15, Nome= "Paraíba", Uf = "PB" },
                        new Estados() { Id = 16, Nome= "Paraná", Uf = "PR" },
                        new Estados() { Id = 17, Nome= "Pernambuco", Uf = "PE" },
                        new Estados() { Id = 18, Nome= "Piauí", Uf = "PI" },
                        new Estados() { Id = 19, Nome= "Rio de Janeiro", Uf = "RJ" },
                        new Estados() { Id = 20, Nome= "Rio Grande do Norte", Uf = "RN" },
                        new Estados() { Id = 21, Nome= "Rio Grande do Sul", Uf = "RS" },
                        new Estados() { Id = 22, Nome= "Rondônia", Uf = "RO" },
                        new Estados() { Id = 23, Nome= "Roraima", Uf = "RR" },
                        new Estados() { Id = 24, Nome= "Santa Catarina", Uf = "SC" },
                        new Estados() { Id = 25, Nome= "São Paulo", Uf = "SP" },
                        new Estados() { Id = 26, Nome= "Sergipe", Uf = "SE" },
                        new Estados() { Id = 27, Nome= "Tocantins", Uf = "TO" });

        }

    }
}