namespace estados
{
    public class Estados
    {
        
        public int Id  { get; set; }

        public string Uf { get; set; }
        
        public string Nome { get; set; }
    }
}