using System.Collections.Immutable;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace estados
{
    [ApiController]
    [Route("[controller]")]
    public class EstadosController : Controller
    {
        private readonly EstadoDbContext _context;
        
        public EstadosController(EstadoDbContext context)
        {
            _context = context;
           
        }


        [HttpGet]   
        public ImmutableList<Estados> Get()
        {
            return _context.Estados.ToImmutableList();
        }

    }
}